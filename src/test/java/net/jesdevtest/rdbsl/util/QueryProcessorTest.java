package net.jesdevtest.rdbsl.util;

import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@RunWith(MockitoJUnitRunner.class)
public class QueryProcessorTest {

@Mock private NamedParameterJdbcTemplate jdbcTemplate;

QueryProcessor subject = new QueryProcessor();

@Test
public void shouldRemoveBadChars() {
  subject.selectFromSchema(jdbcTemplate, "'test='", "'1=1&", Optional.empty());

  Mockito.verify(jdbcTemplate).query(Mockito.eq("SELECT * FROM test.11 "), Mockito.any(RowMapper.class));
}

@Test
public void shouldNotRunOnEmptySchemaOrTable() {
  subject.selectFromSchema(jdbcTemplate, "", "test", Optional.empty());

  Mockito.verify(jdbcTemplate, Mockito.never()).query(Mockito.anyString(), Mockito.any(RowMapper.class));

  subject.selectFromSchema(jdbcTemplate, "test", "", Optional.empty());

  Mockito.verify(jdbcTemplate, Mockito.never()).query(Mockito.anyString(), Mockito.any(RowMapper.class));

  subject.selectFromSchema(jdbcTemplate, "", "", Optional.empty());

  Mockito.verify(jdbcTemplate, Mockito.never()).query(Mockito.anyString(), Mockito.any(RowMapper.class));
}

}
