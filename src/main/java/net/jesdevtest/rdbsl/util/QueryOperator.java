package net.jesdevtest.rdbsl.util;

public enum QueryOperator {
  EQ("="),
  GT(">"),
  LT("<"),
  GE(">="),
  LE("<="),
  LIKE("LIKE");
  private String strVal;

  QueryOperator(String strVal) {
    this.strVal = strVal;
  }

  @Override
  public String toString() {
    return strVal;
  }
}
