package net.jesdevtest.rdbsl.dto;

import net.jesdevtest.rdbsl.util.QueryOperator;

public class QueryFilter {

  private String field;
  private QueryOperator operator;
  private Object value;

  public QueryFilter(String field, QueryOperator operator, Object value) {
    this.field = field;
    this.operator = operator;
    this.value = value;
  }

  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public QueryOperator getOperator() {
    return operator;
  }

  public void setOperator(QueryOperator operator) {
    this.operator = operator;
  }

  public Object getValue() {
    return value;
  }

  public void setValue(Object value) {
    this.value = value;
  }
}
