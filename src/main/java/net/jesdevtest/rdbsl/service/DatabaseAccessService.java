package net.jesdevtest.rdbsl.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import net.jesdevtest.rdbsl.dto.QueryFilter;
import net.jesdevtest.rdbsl.util.QueryProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class DatabaseAccessService {

  private final NamedParameterJdbcTemplate jdbcTemplate;

  @Autowired
  public DatabaseAccessService(final NamedParameterJdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public List<Map<String, Object>> selectFromSchema(final String schema, final String table,
      final Optional<List<QueryFilter>> filters) {

    return QueryProcessor.getInstance().selectFromSchema(jdbcTemplate, schema, table, filters);

  }
}